package com.mygame.gateway.message.channel;

public interface GameChannelInitializer {

    void initChannel(GameChannel channel);
}
