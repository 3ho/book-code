package com.mygame.im;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import com.mygame.game.messagedispatcher.DispatchGameMessageService;
import com.mygame.gateway.message.context.GatewayMessageConsumerService;
import com.mygame.gateway.message.context.ServerConfig;
import com.mygame.gateway.message.handler.GameChannelIdleStateHandler;
import com.mygame.im.handler.GameIMHandler;
@SpringBootApplication(scanBasePackages = {"com.mygame"})
@EnableMongoRepositories(basePackages = {"com.mygame"}) // 负责连接数据库
public class GameIMMain {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(GameIMMain.class, args);//初始化spring boot环境
        ServerConfig serverConfig = context.getBean(ServerConfig.class);//获取配置的实例
        DispatchGameMessageService.scanGameMessages(context, serverConfig.getServiceId(), "com.mygame");// 扫描此服务可以处理的消息
        GatewayMessageConsumerService gatewayMessageConsumerService = context.getBean(GatewayMessageConsumerService.class);//获取网关消息监听实例         
        gatewayMessageConsumerService.start((gameChannel) -> {//启动网关消息监听，并初始化GameChannelHandler
            // 初始化channel
            gameChannel.getChannelPiple().addLast(new GameChannelIdleStateHandler(300, 300, 300));
            gameChannel.getChannelPiple().addLast(new GameIMHandler (context));
        },serverConfig.getServerId());
    }
}
