package com.mygame.xinyue.service;

import static org.testng.Assert.assertEquals;

import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

@SpringBootTest(classes = {TestSpyBean.class})
@TestExecutionListeners(listeners = MockitoTestExecutionListener.class)//必须有这个注解，要不然@SpyBean和@MockBean标记的类会为null
@PowerMockIgnore({"org.springframework.*","javax.*","org.mockito.*"})
@PrepareForTest(TestSpyBean.class)
public class ResetMockTest extends AbstractTestNGSpringContextTests{
	@SpyBean
	private TestSpyBean testSpyBean;//注入要测试的类,使用SpyBean标记
    @MockBean
    private TestMockBean testMockBean; //注入要测试的类，使用MockBean标记
    @AfterMethod
    public void setUp() {
    	Mockito.reset(testSpyBean);
    }
    @Test
    public void testGetValue() {
        //不指定返回直接，直接调用
        int value = testSpyBean.getValue();
        assertEquals(value, 3);
        int value2 = testMockBean.getValue();
        assertEquals(value2, 0);//这里会失败，因为没有指定返回值，value2的值是默认值0
    }
    @Test
    public void testGetSpecialValue() {
        PowerMockito.doReturn(30).when(testSpyBean).getValue();
        int value = testSpyBean.getValue();
        assertEquals(value, 30);
    }
}
