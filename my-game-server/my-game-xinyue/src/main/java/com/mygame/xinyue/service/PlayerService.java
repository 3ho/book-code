package com.mygame.xinyue.service;

import java.util.concurrent.ConcurrentHashMap;
import com.mygame.db.entity.Player;

public class PlayerService {

	private ConcurrentHashMap<Long, Player> playerCache = new ConcurrentHashMap<Long, Player>();
	
	public Player getPlayer(Long playerId) {
		return playerCache.get(playerId);
	}
	
	public void addPlayer(Player player) {
		this.playerCache.putIfAbsent(player.getPlayerId(), player);
	}
	
}
