package com.mygame.xinyue.logic.functionevent;

import org.springframework.context.ApplicationEvent;
import com.mygame.db.entity.manager.PlayerManager;

public class ConsumeGoldEvent extends ApplicationEvent{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private int gold;
    private PlayerManager playerManager;
    public ConsumeGoldEvent(Object source,int gold,PlayerManager playerManager) {
        super(source);
        this.gold = gold;
        this.playerManager = playerManager;
    }
    public int getGold() {
        return gold;
    }
    public PlayerManager getPlayerManager() {
        return playerManager;
    }
    
    
    
    

}
