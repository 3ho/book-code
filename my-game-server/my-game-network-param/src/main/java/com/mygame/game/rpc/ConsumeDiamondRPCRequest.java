package com.mygame.game.rpc;

import com.mygame.game.common.AbstractJsonGameMessage;
import com.mygame.game.common.EnumMesasageType;
import com.mygame.game.common.GameMessageMetadata;
import com.mygame.game.rpc.ConsumeDiamondRPCRequest.RequestBody;

@GameMessageMetadata(messageId = 204, messageType = EnumMesasageType.RPC_REQUEST, serviceId = 101)
public class ConsumeDiamondRPCRequest extends AbstractJsonGameMessage<RequestBody> {
    public static class RequestBody {
        private int consumeCount;

        public int getConsumeCount() {
            return consumeCount;
        }
        public void setConsumeCount(int consumeCount) {
            this.consumeCount = consumeCount;
        }
    }

    @Override
    protected Class<RequestBody> getBodyObjClass() {
        return RequestBody.class;
    }



}
