package com.mygame.game.rpc;

import com.mygame.game.common.AbstractJsonGameMessage;
import com.mygame.game.common.EnumMesasageType;
import com.mygame.game.common.GameMessageMetadata;
import com.mygame.game.rpc.ConsumeDiamonRPCResponse.ResponseBody;

@GameMessageMetadata(messageId = 204, messageType = EnumMesasageType.RPC_RESPONSE, serviceId = 102) // 返回的服务id是102服务
public class ConsumeDiamonRPCResponse extends AbstractJsonGameMessage<ResponseBody> {
    public static class ResponseBody {
        
    }

    @Override
    protected Class<ResponseBody> getBodyObjClass() {
        return null;
    }
}
