package com.mygame.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.mygame.db.entity.Player;

public interface PlayerRepository extends MongoRepository<Player, Long> {

}
