package com.mygame.test;
/**
 * 
 * @ClassName: AbstractXinyueIntegrationTest
 * @Description: 集成测试抽象公共类
 * @author: wgs
 * @date: 2019年7月15日 下午2:55:47
 */

import static org.testng.Assert.fail;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeClass;
import com.mygame.game.common.GameMessageHeader;
import com.mygame.game.common.IGameMessage;
import com.mygame.game.messagedispatcher.DispatchGameMessageService;
import com.mygame.gateway.message.channel.AbstractGameChannelHandlerContext;
import com.mygame.gateway.message.context.GatewayMessageContext;
import com.mygame.gateway.message.context.ServerConfig;

public class AbstractXinyueIntegrationTest<T> extends AbstractXinyueUnitTest {
    @Autowired
    private DispatchGameMessageService dispatchGameMessageService;//消息转化到执行方法的service类。这个类在项目中是接收完客户端消息之后，在channel的pipeline中最
    //后一个Handler中分发消息的。
    @Autowired
    private ServerConfig serverConfig; //服务本地配置
    private int seqId; //消息发送的序列id
    @BeforeClass
    public void superInit() {//在测试类启动的时候，先扫描一下服务中的请求类和处理类的映射。
        DispatchGameMessageService.scanGameMessages(applicationContext, 0, "com.mygame");// 扫描此服务可以处理的消息
    }
    public void sendGameMessage(long playerId,T data,IGameMessage request,Consumer<IGameMessage> responseConsumer) {
        GameMessageHeader header = request.getHeader();
        header.setPlayerId(playerId);
        header.setClientSeqId(this.seqId ++);
        header.setClientSendTime(System.currentTimeMillis());
        header.setFromServerId(serverConfig.getServerId());
        CountDownLatch countDownLatch = new CountDownLatch(1);//因为发送消息是异常的，所以这里使用CountDownLatch保证测试代码的同步性
        AbstractGameChannelHandlerContext  gameChannelHandlerContext = Mockito.mock(AbstractGameChannelHandlerContext.class);//Mock一下Channel的上下文在doAnswer中验证结果
        GatewayMessageContext<T> stx = new GatewayMessageContext<>(data, null, null, request, gameChannelHandlerContext);
        Mockito.doAnswer(c->{//验证请求的返回结果
            IGameMessage gameMessage = c.getArgument(0);
            responseConsumer.accept(gameMessage);
            countDownLatch.countDown();//当消息返回时，放开下面await的阻塞
          return null;  
        }).when(gameChannelHandlerContext).writeAndFlush(Mockito.any());//对这个方法进行mock，当处理完消息，向客户端返回时，在doAnswer中验证返回结果
        dispatchGameMessageService.callMethod(request, stx);//调用处理消息的方法
        try {
           boolean result = countDownLatch.await(30,TimeUnit.SECONDS);//这里阻塞30秒，等待处理结果返回
           if(!result) {
               fail("请求超时，超时时间30秒，请求：" + request.getClass().getName());
           }
        } catch (InterruptedException e) {//如果异常，则测试失败
            fail("测试失败", e);
        }
    } 
}
